
local func = require("scripts.func")

function playercontroller(player, dt)
   position = player.position
   speed = player.speed
   local dir = {x = 0, y = 0}
   if love.keyboard.isDown('d') and not love.keyboard.isDown('a') then
      dir.x = 1 end
   if love.keyboard.isDown('a') and not love.keyboard.isDown('d') then
      dir.x = -1 end
   if love.keyboard.isDown('w') and not love.keyboard.isDown('s') then
      dir.y = -1 end
   if love.keyboard.isDown('s') and not love.keyboard.isDown('w') then
      dir.y = 1 end
   l = math.sqrt(dir.x * dir.x + dir.y * dir.y)
   v = {x = dir.x ~= 0 and dir.x / l or 0, y = dir.y ~= 0 and (dir.y / l) / 2 or 0}
   player.velocity.x = player.velocity.x + dt * 20 * v.x
   player.velocity.y = player.velocity.y + dt * 20 * v.y

   maxspeed = player.maxspeed
   if player.velocity.x > maxspeed then player.velocity.x = maxspeed end
   if player.velocity.x < -maxspeed then player.velocity.x = -maxspeed end
   if player.velocity.y > maxspeed / 2 then player.velocity.y = maxspeed / 2 end
   if player.velocity.y < -maxspeed / 2 then player.velocity.y = -maxspeed / 2 end

   if dir.x == 0 then
      if player.velocity.x > 0 then
         player.velocity.x = player.velocity.x - dt * 50
         if player.velocity.x < 0 then player.velocity.x = 0 end
      elseif player.velocity.x < 0 then
         player.velocity.x = player.velocity.x + dt * 50
         if player.velocity.x > 0 then player.velocity.x = 0 end
      end
   end

   if dir.y == 0 then
      if player.velocity.y > 0 then
         player.velocity.y = player.velocity.y - dt * 50
         if player.velocity.y < 0 then player.velocity.y = 0 end
      elseif player.velocity.y < 0 then
         player.velocity.y = player.velocity.y + dt * 50
         if player.velocity.y > 0 then player.velocity.y = 0 end
      end
   end

   return {x = position.x + player.velocity.x, y = position.y + player.velocity.y}
end

function love.draw()
   playerpos = gamestate.player.position
      
   love.graphics.push()
   love.graphics.translate(math.floor(gamestate.camera.position.x), math.floor(gamestate.camera.position.y))
   love.graphics.translate(-playerpos.x + (love.graphics.getWidth() - 10) / 2, -playerpos.y + (love.graphics.getHeight()  - 10) / 2)
   for i=-1,1 do
      for l=-1,1 do
         drawChunk(i+math.floor(playerpos.x/(world.tile_dimension.x*world.chunk.grid)+playerpos.y/(world.tile_dimension.y*world.chunk.grid)),
                  l+math.floor(playerpos.y/(world.tile_dimension.y*world.chunk.grid)-playerpos.x/(world.tile_dimension.x*world.chunk.grid)))
      end
   end
   drawPlayer(playerpos)
   love.graphics.pop()
   love.graphics.print("x: " .. playerpos.x, 10, 10)
   love.graphics.print("y: " .. playerpos.y, 10, 30)
   love.graphics.print("Cx: " .. math.floor(playerpos.x/(world.tile_dimension.x*world.chunk.grid)+playerpos.y/(world.tile_dimension.y*world.chunk.grid)), 10, 50)
   love.graphics.print("Cy: " .. math.floor(playerpos.y/(world.tile_dimension.y*world.chunk.grid)-playerpos.x/(world.tile_dimension.x*world.chunk.grid)), 10, 70)
   drawPerformancemeter()
end


function drawPerformancemeter()
   love.graphics.setColor(255, 255, 255)
   love.graphics.line(10, love.graphics.getHeight() - 15, 10, love.graphics.getHeight() - 6)
   love.graphics.line(200, love.graphics.getHeight() - 15, 200, love.graphics.getHeight() - 6)
   love.graphics.line(300, love.graphics.getHeight() - 15, 300, love.graphics.getHeight() - 6)
   love.graphics.line(40, love.graphics.getHeight() - 15, 40, love.graphics.getHeight() - 6)
   love.graphics.setColor(120, 80, 80)
   love.graphics.rectangle("fill", 10, love.graphics.getHeight() - 10, love.graphics.getWidth() - 20, 5)
   love.graphics.setColor(0, 255, 0)
   love.graphics.rectangle("fill", 10, love.graphics.getHeight() - 10, gamestate.dt * 10000, 5)
   love.graphics.setColor(255, 255, 255)
end

function drawPlayer(playerpos)
   love.graphics.rectangle("fill", playerpos.x, playerpos.y, 10, 10)

   aimvector = {x = love.mouse.getX() - love.graphics.getWidth() / 2, y = love.mouse.getY() - love.graphics.getHeight() / 2}
   al = math.sqrt(aimvector.x * aimvector.x + aimvector.y * aimvector.y)
   aimunitvector = {x = aimvector.x / al, y = aimvector.y / al}
   viewvector = {x = aimunitvector.x < -0.45 and -1 or aimunitvector.x > 0.45 and 1 or 0,
                 y = aimunitvector.y < -0.25 and -1 or aimunitvector.y > 0.25 and 1 or 0}
   vl = math.sqrt(viewvector.x * viewvector.x + viewvector.y * viewvector.y)
   viewunitvector = {x = viewvector.x / vl, y = viewvector.y / vl * 0.5}
   love.graphics.line(playerpos.x + 5, playerpos.y + 5, aimunitvector.x * 100 + playerpos.x + 5, aimunitvector.y * 100 + playerpos.y + 5)
   love.graphics.setColor(255, 0, 0)
   love.graphics.line(playerpos.x + 5, playerpos.y + 5, viewunitvector.x * 50 + playerpos.x + 5, viewunitvector.y * 50 + playerpos.y + 5)
   love.graphics.setColor(255, 255, 255)

end

function love.update(dt)
   if love.keyboard.isDown("escape") then
      love.event.quit()
   end

   gamestate.player.position = updatePosition(gamestate.player, dt)
   gamestate.dt = dt
   
end

-- eh, virker ikke helt som det skal
vector = {
   fromto = function(p1, p2)
      local x = p1.x + p2.x
      local y = p1.y + p2.y
      return {x = x, y = y}
   end,

   tounit = function(v)
      local l = math.sqrt(v.x * v.x + v.y * v.y)
      return {x = v.x / l, y = v.y / l}
   end
}

function updatePosition(entity, dt)
   local contfn = entity.controllerfn
   local pos = entity.position
   local speed = entity.speed
   return contfn(entity, dt)
end

function drawChunk(_x, _y)
   x_offset =  ((tonumber(_x) - tonumber(_y)) * world.chunk.grid * world.tile_dimension.x/2)
   y_offset =  ((tonumber(_x) + tonumber(_y)) * world.chunk.grid * world.tile_dimension.y/2)
   for i=0,world.chunk.grid-1 do
      for l=0,world.chunk.grid-1 do
         love.graphics.draw(world.biomes.stone[(tonumber(_x)%2)+1], 
            gamestate.camera.position.x+((i - l) * world.tile_dimension.x/2)+x_offset, 
            gamestate.camera.position.y+((i + l) * world.tile_dimension.y/2)+y_offset, 0,1,1,0,0)
      end
   end
end

world = {
   seed = 0, -- Ikke i brug pt.
   tile_dimension = {x = 160, y = 80},
   x = 0,
   y = 0,
   biomes = {
      stone = {
         love.graphics.newImage('/assist/stone/groundTile.png'),
         love.graphics.newImage('/assist/stone/groundTile1.png')
      }
   },
   chunk = {
      grid = 7
   }
}

gamestate = {
   dt = 0,
   player = {
      position  = {x = -5, y = -5},
      direction = {x = 0, y = 1},
      velocity = {x = 0, y = 0},
      maxspeed = 5,
      speed = 600,
      controllerfn = playercontroller
   },

   camera = {
      position = {x = 0, y = 0}
   }
}
