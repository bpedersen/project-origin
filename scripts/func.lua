

local function reduce(fn, initial, list)
      local ret = initial
   for _, elem in pairs(list) do
      ret = fn(ret, elem)
   end
   return ret
end

local function map(fn, list)
   local f = function(acc, el) table.insert(acc, fn(el)); return acc end
   return reduce(f, {}, list)
end

local function filter(fn, list)
   local f = function(acc, el)
      if fn(el) then
         table.insert(acc, el)
         return acc
      else
         return acc
      end
   end
   return reduce(f, {}, list)
end

return {
   reduce = reduce,
   map = map,
   filter = filter
}
